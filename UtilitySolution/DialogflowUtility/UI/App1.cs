﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Com.AutomationAnywhere.WorkLoadManagement.Apis.Util;

namespace Com.AutomationAnywhere.Utility.Dialogflow.UI
{
    public partial class App1 : Form
    {
        public App1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            /*
            var user_name = "msteamqueue";
            var password = "Admin@12345";
            var controlRoomUrl = "https://aa-saleseng-us-4sbx.cloud.automationanywhere.digital";
            var token = await WorkLoadManagementApisUtil.Instance.GetAccessTokenAsync(controlRoomUrl, user_name, password);
            var array = await WorkLoadManagementApisUtil.Instance.GetWorkQueuesAsync(controlRoomUrl, token);
            var queueModel = await getQueueId("Teams_Queue", array);
            var dataTemplate = await WorkLoadManagementApisUtil.Instance.GetWorkQueueModelAsync(controlRoomUrl, token, queueModel.WorkItemId);
            dataTemplate = dataTemplate.Replace("{1}", "101").Replace("{3}", "Your service sucks").Replace("{2}", "Admin");
            var ids = await WorkLoadManagementApisUtil.Instance.AddWorkItemToQueueAsync(controlRoomUrl, token, queueModel.Id, dataTemplate);
            WorkResult result = null;
            do
            {
                result = await WorkLoadManagementApisUtil.Instance.GetWorkResultAsync(controlRoomUrl, token, queueModel.Id, (ids.ToArray())[0]);
                Thread.Sleep(3000);
            } while (result.Status != "COMPLETED" && result.Status != "FAILED");
            */

            var user_name = "himanshu_run";
            var password = "Admin@12345";
            var controlRoomUrl = "https://aa-se-ind-5.my.automationanywhere.digital";
            var token = await WorkLoadManagementApisUtil.Instance.GetAccessTokenAsync(controlRoomUrl, user_name, password);
            var deploymentId = await WorkLoadManagementApisUtil.Instance.DeployBotAsync(controlRoomUrl, token, "214719", "372", "155", 
                "\"strAccountNumber\":{\"type\":\"STRING\", \"string\":\"9150411123544\"}");
            var executionId = await WorkLoadManagementApisUtil.Instance.GetExecutionIdAsync(controlRoomUrl, token, deploymentId);
            WorkResult result = null;
            do 
            {
                result = await WorkLoadManagementApisUtil.Instance.GetDeployBotOutputAsync(controlRoomUrl, token, executionId);
            } while (result.Status != "COMPLETED" && result.Status != "FAILED");

            MessageBox.Show(result.Result.ToString());

        }

        private async Task<QueueModel> getQueueId(String queueName, IEnumerable<QueueModel> model)
        {
            return await Task.Run<QueueModel>(() => {
                QueueModel result = null;
                foreach(var item in model)
                {
                    if (queueName.Equals(item.Name))
                    {
                        result = item;
                        break;
                    }
                }
                return result;
            });
        }
    }
}

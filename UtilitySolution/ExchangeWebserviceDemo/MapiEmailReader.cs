﻿using Microsoft.Office.Interop.Outlook;
using System.Runtime.InteropServices;
using System.Reflection;

namespace ExchangeWebserviceDemo
{
    internal class MapiEmailReader
    {
        private Application outlookApplication = null;
        private NameSpace outlookNameSpace = null;
        private readonly MAPIFolder inboxFolder = null;

        private enum MailReadStatus { READ, UNREAD, ALL};

        public MapiEmailReader(string customFolderName = "Inbox")
        {
            outlookApplication = new Application();
            outlookNameSpace = outlookApplication.GetNamespace("MAPI");
            var currentProfile = outlookNameSpace.GetType().InvokeMember("CurrentProfileName", BindingFlags.GetProperty, null, outlookNameSpace, null);
            outlookNameSpace.Logon(currentProfile, Missing.Value, false, true);
            inboxFolder = outlookNameSpace.GetDefaultFolder(OlDefaultFolders.olFolderInbox);
            if (!"inbox".Equals(customFolderName.ToLower()))
                inboxFolder = GetCustomInbox(inboxFolder, customFolderName);
        }
        private Items GetEmails(MailReadStatus status)
        {
            var items = inboxFolder.Items;
            switch (status)
            {
                case MailReadStatus.READ:
                    items = items.Restrict("[Unread]=false");
                    break;
                case MailReadStatus.UNREAD:
                    items = items.Restrict("[Unread]=true");
                    break;
            }
            items.Sort("[ReceivedTime]", true);
            return items;
        }

        public Items GetReadEmails() { return GetEmails(MailReadStatus.READ); }

        public Items GetUnreadEmails() { return GetEmails(MailReadStatus.UNREAD); }

        public Items GetAllEmails() { return GetEmails(MailReadStatus.ALL); }

        public MailItem GetLatestEmail()
        {
            return inboxFolder.Items.GetLast();
        }

        private static MAPIFolder GetCustomInbox(MAPIFolder inboxFolder, string customFolderName)
        {
            MAPIFolder inbox = null;
            foreach(MAPIFolder folder in inboxFolder.Folders)
            {
                if(folder.Name == customFolderName)
                {
                    inbox = folder;
                    break;
                }
            }
            return inbox;
        }

        private static void ReleaseComObject(object obj)
        {
            if (obj != null)
            {
                Marshal.ReleaseComObject(obj);
                obj = null;
            }
        }

        ~MapiEmailReader()
        {
            ReleaseComObject(inboxFolder);
            outlookNameSpace.Logoff();
            ReleaseComObject(outlookNameSpace);
            ReleaseComObject(outlookApplication);
        }
    }
}

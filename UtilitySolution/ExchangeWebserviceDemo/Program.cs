﻿using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Office.Interop.Outlook;

namespace ExchangeWebserviceDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013_SP1);
                ////service.Credentials = new WebCredentials("himanshu.manjarawala@automationanywhere.com", "g0dF@theR04");
                //service.Credentials = new WebCredentials("h.manjarawala@hotmail.com", "gr@ndf@ther");
                //service.TraceEnabled = true;
                //service.TraceFlags = TraceFlags.All;
                //service.Url = new Uri("https://outlook.office365.com/EWS/Exchange.asmx");
                ////service.AutodiscoverUrl("himanshu.manjarawala@automationanywhere.com", RedirectionUrlValidationCallback);
                //service.UseDefaultCredentials = false;
                //service.WebProxy = null;

                //if(service != null)
                //{
                //    ItemView view = new ItemView(30);
                //    FindItemsResults<Item> findResults = service.FindItems(WellKnownFolderName.Inbox, view);
                //    foreach(var item in findResults)
                //    {

                //    }
                //}

                MapiEmailReader reader = new MapiEmailReader(ConfigurationManager.AppSettings["MailFolderName"]);
                //MailItem mailItem = reader.GetLatestEmail();
                //extractMailInfo(mailItem);
                string path = ConfigurationManager.AppSettings["AttachmentFolderPath"];
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                int nCounter = 0;
                var items = reader.GetUnreadEmails();
                WriteToFile(DateTime.Now + "\t" + items.Count + " unread email(s) found in Inbox.");
                foreach (MailItem item in items)
                {
                    extractMailInfo(item);
                    nCounter++;
                    //if (nCounter > 4) break;
                }
                if(nCounter == 0) { Console.WriteLine("There are no unread emails to show!!"); }
            }
            catch (System.Exception ex)
            {
                WriteToFile(DateTime.Now + "\tException occurs " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            //Console.ReadKey();
        }

        private static void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        private static void extractMailInfo(MailItem item)
        {
            Regex regX = new Regex(@"^.*\.(" + ConfigurationManager.AppSettings["AllowedFileTypes"] +")$", RegexOptions.IgnoreCase);
            Console.WriteLine("============================================================");
            Console.WriteLine(String.Concat("Email From: ", item.SenderEmailAddress));
            Console.WriteLine(String.Concat("Email To: ", item.To));
            Console.WriteLine(String.Concat("Subject: ", item.Subject));
            Console.WriteLine(String.Concat("Date: ", item.ReceivedTime));
            item.UnRead = false;
            foreach (Attachment attachment in item.Attachments)
            {
                Console.WriteLine(attachment.FileName);
                if (regX.IsMatch(attachment.FileName))
                {
                    attachment.SaveAsFile(ConfigurationManager.AppSettings["AttachmentFolderPath"] + @"\" + attachment.FileName);
                    WriteToFile(DateTime.Now + "\tAttachment " + attachment.FileName + " downloaded");
                }
                else
                {
                    WriteToFile(DateTime.Now + "\tAttachment " + attachment.FileName + " doesn\'t complies with allowed file types.");
                }
            }
            Console.WriteLine("============================================================");
        }

        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;
            Uri redirectionUri = new Uri(redirectionUrl);
            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }
    }
}

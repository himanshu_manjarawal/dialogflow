﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.AutomationAnywhere.WorkLoadManagement.Apis.Util
{
    public class WorkResult
    {
        public string Status { get; private set; }

        public IDictionary<string, object> Result { get; private set; }

        public WorkResult(string status, string result)
        {
            Status = status;
            Result = new Dictionary<string, object> { ["Result"] = result };
        }

        public WorkResult(string status, IDictionary<string, object> result)
        {
            Status = status;
            Result = result;
        }

        public override string ToString()
        {
            return Result.ToString();
        }
    }
}

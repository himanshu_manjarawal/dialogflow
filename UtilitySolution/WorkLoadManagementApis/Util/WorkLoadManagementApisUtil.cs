﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Com.AutomationAnywhere.WorkLoadManagement.Apis.Util
{
    public sealed class WorkLoadManagementApisUtil
    {
        static WorkLoadManagementApisUtil _instance;

        private static object _instanceLock = new object();

        private WorkLoadManagementApisUtil() { }

        public static WorkLoadManagementApisUtil Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_instanceLock)
                    {
                        if (_instance == null)
                        {
                            _instance = new WorkLoadManagementApisUtil();
                        }
                    }
                }
                return _instance;
            }
        }

        public string GetAccessToken(string controlRoomUrl, string username, string password)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v1/authentication");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            var data = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\"}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                return jsonObject.GetValue("token").ToString();
            }
            return null;
        }

        public async Task<string> GetAccessTokenAsync(String controlRoomUrl, String username, String password)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v1/authentication");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            var data = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\"}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                return jsonObject.GetValue("token").ToString();
            }
            return null;
        }

        #region WLM APIs

        public IEnumerable<QueueModel> GetWorkQueues(String controlRoomUrl, String token)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/wlm/queues/list");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = "{\"sort\":[{\"field\":\"name\",\"direction\":\"asc\"}]," +
                "\"filter\":{\"operator\": \"eq\", \"value\": \"IN_USE\", \"field\": \"status\"},\"fields\":[]," +
                "\"page\":{\"length\":100,\"offset\":0}}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                var array = jsonObject.GetValue("list").ToArray();
                return getComboItemArray(array);
            }
            return null;
        }

        public async Task<IEnumerable<QueueModel>> GetWorkQueuesAsync(String controlRoomUrl, String token)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/wlm/queues/list");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = "{\"sort\":[{\"field\":\"name\",\"direction\":\"asc\"}]," +
                "\"filter\":{\"operator\": \"eq\", \"value\": \"IN_USE\", \"field\": \"status\"},\"fields\":[]," +
                "\"page\":{\"length\":100,\"offset\":0}}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                var array = jsonObject.GetValue("list").ToArray();
                return await getComboItemArrayAsync(array);
            }
            return null;
        }

        public string GetWorkQueueModel(String controlRoomUrl, String token, String workItemId)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/wlm/workitemmodels/list");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = "{}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                var array = jsonObject.GetValue("list").ToArray();
                return getWorkItemModel(array, workItemId);
            }
            return null;
        }

        public async Task<string> GetWorkQueueModelAsync(String controlRoomUrl, String token, String workItemId)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/wlm/workitemmodels/list");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = "{}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                var array = jsonObject.GetValue("list").ToArray();
                return await getWorkItemModelAsync(array, workItemId);
            }
            return null;
        }

        public IEnumerable<string> AddWorkItemToQueue(String controlRoomUrl, String token, String queueId, String workItem)
        {
            string apiUrl = string.Concat(controlRoomUrl, $"/v3/wlm/queues/{queueId}/workitems");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            request.AddParameter("application/json", workItem, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                var array = jsonObject.GetValue("list").ToArray();
                return getWorkItemIds(array);
            }
            return null;
        }

        public async Task<IEnumerable<string>> AddWorkItemToQueueAsync(String controlRoomUrl, String token, String queueId, String workItem)
        {
            string apiUrl = string.Concat(controlRoomUrl, $"/v3/wlm/queues/{queueId}/workitems");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            request.AddParameter("application/json", workItem, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                var jsonObject = JObject.Parse(response.Content);
                var array = jsonObject.GetValue("list").ToArray();
                return await getWorkItemIdsAsync(array);
            }
            return null;
        }

        public WorkResult GetWorkResult(String controlRoomUrl, String token, String queueId, String workItemId)
        {
            string apiUrl = string.Concat(controlRoomUrl, $"/v3/wlm/queues/{queueId}/workitems/{workItemId}");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                return new WorkResult(jsonObject.Value<String>("status"), jsonObject.Value<String>("result"));
            }
            return null;
        }

        public async Task<WorkResult> GetWorkResultAsync(String controlRoomUrl, String token, String queueId, String workItemId)
        {
            string apiUrl = string.Concat(controlRoomUrl, $"/v3/wlm/queues/{queueId}/workitems/{workItemId}");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                return new WorkResult(jsonObject.Value<String>("status"), jsonObject.Value<String>("result"));
            }
            return null;
        }

        private static string getWorkItemModel(JToken[] array, String workItemId)
        {
            String result = String.Empty;
            foreach (JToken item in array)
            {
                String id = item.Value<String>("id");
                if (id.Equals(workItemId))
                {
                    result = getWorkItemString(item as JObject);
                }
            }
            return result;
        }

        private static async Task<String> getWorkItemModelAsync(JToken[] array, String workItemId)
        {
            return await Task.Run<String>(async () =>
            {
                String result = String.Empty;
                foreach (JToken item in array)
                {
                    String id = item.Value<String>("id");
                    if (id.Equals(workItemId))
                    {
                        result = await getWorkItemStringAsync(item as JObject);
                    }
                }
                return result;
            });
        }

        private static string getWorkItemString(JObject @object)
        {
            var attributes = @object.GetValue("attributes").ToArray();
            StringBuilder builder = new StringBuilder();
            String result = "{\"workItems\":[{\"json\":{TEMPLATE}}]}";
            int counter = 0;
            foreach (JToken attribute in attributes)
            {
                counter++;
                String attrib_name = attribute.Value<String>("name");
                String attrib_type = attribute.Value<String>("type");

                if ("NUMBER".Equals(attrib_type))
                {
                    builder.Append($", \"{attrib_name}\": {{{counter}}}");
                }
                else
                {
                    builder.Append($", \"{attrib_name}\": \"{{{counter}}}\"");
                }
            }
            return result.Replace("TEMPLATE", builder.ToString().Substring(1).Trim());
        }

        private static async Task<String> getWorkItemStringAsync(JObject @object)
        {
            return await Task.Run<String>(() => { return getWorkItemString(@object); });
        }

        private static IEnumerable<QueueModel> getComboItemArray(JToken[] array)
        {
            var result = new QueueModel[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                var token = array[i];
                result[i] = new QueueModel(token.Value<String>("id"), token.Value<String>("name"), token.Value<String>("workItemModelId"));
            }
            return result;
        }

        private static async Task<IEnumerable<QueueModel>> getComboItemArrayAsync(JToken[] array)
        {
            var t = await Task.Run<IEnumerable<QueueModel>>(() =>
            {
                return getComboItemArray(array);
            });
            return t;
        }

        private static IEnumerable<String> getWorkItemIds(JToken[] array)
        {
            var result = new string[array.Length];
            int i = 0;
            foreach (var item in array)
            {
                result[i++] = item.Value<String>("id");
            }
            return result;
        }

        private static async Task<IEnumerable<String>> getWorkItemIdsAsync(JToken[] array)
        {
            var t = await Task.Run<IEnumerable<String>>(() => { return getWorkItemIds(array); });
            return t;
        }

        #endregion WLM APIs

        #region Deploy Bot APIs

        public string DeployBot(String controlRoomUrl, String token, String botID, String runAsUserID, String poolID, String botInput)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/automations/deploy");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = $"{{\"fileId\":\"{botID}\", \"botInput\":{{{botInput}}}, \"poolIds\":[{poolID}], \"runAsUserIds\":[{runAsUserID}]}}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                return jsonObject.GetValue("deploymentId").ToString();
            }
            return null;
        }

        public async Task<string> DeployBotAsync(String controlRoomUrl, String token, String botID, String runAsUserID, String poolID, String botInput)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/automations/deploy");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = $"{{\"fileId\":\"{botID}\", \"botInput\":{{{botInput}}}, \"poolIds\":[{poolID}], \"runAsUserIds\":[{runAsUserID}]}}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                return jsonObject.GetValue("deploymentId").ToString();
            }
            return null;
        }

        public string GetExecutionId(String controlRoomUrl, String token, String deploymentID)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/activity/list");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = $"{{\"sort\":[{{\"field\": \"endDateTime\", \"direction\": \"desc\"}}], \"filter\":{{\"operator\":\"eq\",\"field\":\"deploymentId\",\"value\":\"{deploymentID}\"}}}}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                var jsonObject1 = jsonObject.GetValue("list").ToArray()[0] as JObject;
                return jsonObject1.GetValue("id").ToString();
            }
            return null;
        }

        public async Task<string> GetExecutionIdAsync(String controlRoomUrl, String token, String deploymentID)
        {
            string apiUrl = string.Concat(controlRoomUrl, "/v3/activity/list");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            var data = $"{{\"sort\":[{{\"field\": \"endDateTime\", \"direction\": \"desc\"}}], \"filter\":{{\"operator\":\"eq\",\"field\":\"deploymentId\",\"value\":\"{deploymentID}\"}}}}";
            request.AddParameter("application/json", data, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                var jsonObject1 = jsonObject.GetValue("list").ToArray()[0] as JObject;
                return jsonObject1.GetValue("id").ToString();
            }
            return null;
        }

        public WorkResult GetDeployBotOutput(String controlRoomUrl, String token, String executionID)
        {
            string apiUrl = string.Concat(controlRoomUrl, $"/v3/activity/execution/{executionID}");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                return generateDeployBotResult(jsonObject);
            }
            return null;
        }

        public async Task<WorkResult> GetDeployBotOutputAsync(String controlRoomUrl, String token, String executionID)
        {
            string apiUrl = string.Concat(controlRoomUrl, $"/v3/activity/execution/{executionID}");
            var client = new RestClient(apiUrl);

            client.Timeout = 3000;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Authorization", token);
            IRestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonObject = JObject.Parse(response.Content);
                return await generateDeployBotResultAsync(jsonObject);
            }
            return null;
        }

        private static WorkResult generateDeployBotResult(JObject @object) 
        {
            var status = @object.GetValue("status").ToString();
            if (status == "COMPLETED" || status == "FAILED")
            {
                var jsonObject1 = @object.GetValue("botOutVariables") as JObject;
                return new WorkResult(status, ((JObject)jsonObject1.GetValue("values")).ToDictionary());
            }
            else
            {
                return new WorkResult(status, new Dictionary<string, object>());
            }
        }

        private async static Task<WorkResult> generateDeployBotResultAsync(JObject @object)
        {
            return await Task.Run<WorkResult>(()=> { return generateDeployBotResult(@object); });
        }

        #endregion Deploy Bot APIs
    }
}

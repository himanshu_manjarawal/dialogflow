﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.AutomationAnywhere.WorkLoadManagement.Apis.Util
{
    public class QueueModel
    {
        public String Id { get; private set; }
        public String Name { get; private set; }
        public String WorkItemId { get; private set; }

        public QueueModel(String id, String name, String workItemId)
        {
            this.Id = id;
            this.Name = name;
            this.WorkItemId = workItemId;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}

﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;

namespace ReadOutlookEmailSerive
{
    public partial class Service1 : ServiceBase
    {
        readonly Timer timer = new Timer();
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteToFile(DateTime.Now + "\tService is started.");
            timer.Enabled = true;
            timer.Interval = 10000;
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            GetUnreadEmailsFromOutlook();
        }

        private static void GetUnreadEmailsFromOutlook()
        {
            Application outlookApplication = null;
            NameSpace outlookNameSpace = null;
            MAPIFolder inboxFolder = null;
            
            string attachmentFolder = ConfigurationManager.AppSettings["AttachmentFolderPath"]; //AppDomain.CurrentDomain.BaseDirectory + "\\Attachments";

            try
            {
                outlookApplication = new Application();
                outlookNameSpace = outlookApplication.GetNamespace("MAPI");
                var currentProfile = outlookNameSpace.GetType().InvokeMember("CurrentProfileName", System.Reflection.BindingFlags.GetProperty, null, outlookNameSpace, null);
                outlookNameSpace.Logon(currentProfile, System.Reflection.Missing.Value, false, true);
                inboxFolder = outlookNameSpace.GetDefaultFolder(OlDefaultFolders.olFolderInbox);
                inboxFolder = GetCustomInbox(inboxFolder, ConfigurationManager.AppSettings["MailFolderName"]);

                if (!Directory.Exists(attachmentFolder))
                    Directory.CreateDirectory(attachmentFolder);

                var items = inboxFolder.Items;
                items = items.Restrict("[Unread]=true");
                items.Sort("[ReceivedTime]", true);
                WriteToFile(DateTime.Now + "\t" + items.Count + " unread email(s) found in Inbox.");

                foreach (MailItem item in items)
                {
                    DownloadAttachments(item, attachmentFolder);
                }
            }
            catch (System.Exception ex)
            {
                WriteToFile(DateTime.Now + "\tException occurs " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            finally
            {
                ReleaseComObject(inboxFolder);
                outlookNameSpace.Logoff();
                ReleaseComObject(outlookNameSpace);
                ReleaseComObject(outlookApplication);
            }
        }

        private static void DownloadAttachments(MailItem item, string attachmentFolderPath)
        {
            foreach(Attachment attachment in item.Attachments)
            {
                attachment.SaveAsFile(attachmentFolderPath + "\\" + attachment.FileName);
                WriteToFile(DateTime.Now + "\tAttachment " + attachment.FileName + " downloaded");
            }
        }

        private static MAPIFolder GetCustomInbox(MAPIFolder inboxFolder, string customFolderName)
        {
            if("inbox".Equals(customFolderName.ToLower())) return inboxFolder;
            MAPIFolder inbox = null;
            foreach (MAPIFolder folder in inboxFolder.Folders)
            {
                if (folder.Name == customFolderName)
                {
                    inbox = folder;
                    break;
                }
            }
            return inbox;
        }

        private static void ReleaseComObject(object obj)
        {
            if (obj != null)
            {
                Marshal.ReleaseComObject(obj);
                obj = null;
            }
        }

        private static void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        protected override void OnStop()
        {
            WriteToFile(DateTime.Now + "\tService is stopped.");
        }
    }
}
